#ifndef JSON_H
#define JSON_H
#include <cstddef>

typedef struct lept_value lept_value;
typedef struct lept_member lept_member;

/*json结构体的具体类型*/
/*包括 空类型 布尔值类型 数字类型 字符串类型 数组类型 对象类型*/
typedef enum {LEPT_NULL,LEPT_TRUE,LEPT_FALSE,LEPT_NUMBER,LEPT_STRING,LEPT_ARRAY,LEPT_OBJECT}lept_type;

struct lept_value
{  
	union   //联合体
	{
		struct {
			lept_value *element;
			size_t size;
		}array;//存储数组类型

		struct {
			char *s;
			size_t len;
		}str;//存储字符串类型

		struct{
			lept_member *member;
			size_t size;
		}object;//存储对象类型

		double n;//存储数字类型
	}u;
	
	lept_type type;
};//json结构体

struct lept_member  //键值对结构体
{
	char *key;
	lept_value value;
	size_t keylen;
};

typedef struct
{
	const char* json;
	char *stack;
	size_t size;
	size_t top;
}lept_context;//临时存储输入的json文本内容


enum {
	LEPT_PARSE_OK = 0, //输入json格式合法
	LEPT_PARSE_EXPECT_VALUE,//输入的字符串只包含空白
	LEPT_PARSE_INVALID_VALUE,//输入格式非法
	LEPT_PARSE_ROOT_NOT_SINGULAR,//空白之后有其他字符
	LEPT_PARSE_INVALID_UNICODE_HEX,//非法UTF8字符
	LEPT_PARSE_INVALID_UNICODE_SURROGATE,
	LEPT_PARSE_INVALID_STRING_CHAR,//非法字符串
	LEPT_PARSE_MISS_COMMA_OR_SQUARE_BRACKET,//非法数组
	LEPT_PARSE_MISS_KEY,//键值对缺少关键字
	LEPT_PARSE_MISS_COLON,//键值对缺少:
	LEPT_PARSE_MISS_COMMA_OR_CURLY_BRACKET,//键值对缺少标点
	LEPT_STRINGLY_OK   //有效字符串化
};

/*###########接口##########*/

/*根据输入的字符串获取json结构体*/
int lept_parse(lept_value*v, const char *json);

lept_type lept_get_type(const lept_value *v);
double lept_get_number(const lept_value *v);
char *lept_get_string_firstadd(const lept_value *v);//字符串首地址
size_t lept_get_string_size(const lept_value*v);//字符串大小
void lept_init(lept_value *v);
void lept_free(lept_value *v);
size_t lept_get_array_size(const lept_value *v);//得到json保存的数组大小
lept_value *lept_get_element(const lept_value*v,int cno);//数组首元素
int lept_parse_value(lept_context *c, lept_value *v);
size_t lept_get_object_size(const lept_value *v);
lept_member *lept_get_object_member(const lept_value *v, int cno);

/*根据输入的json结构体格式化得到对应的字符串*/
char *lept_stringly(const lept_value *v);


int lept_stringly_value(lept_context *c, const lept_value *v);

#endif // !JSON_H

