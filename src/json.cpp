#include"json.h"
#include<assert.h>
#include<stdlib.h>
#include<string.h>
#include<stdio.h>

#define EXPECT(c,ch)do{assert(*c->json==(ch));c->json++;}while(0)

static void lept_parse_whitespace(lept_context *c)//解析空白值
{
	const char *p = c->json;
	while (*p == ' ' || *p == '\t' || *p == '\r')
	{ 
		p++;
	}
	c->json = p;

}

static int lept_parse_null(lept_context *c, lept_value *v)//解析null值
{
	EXPECT(c, 'n');
	if (c->json[0] != 'u' || c->json[1] != 'l' || c->json[2] != 'l')
		return LEPT_PARSE_INVALID_VALUE;
	c->json += 3;
	v->type = LEPT_NULL;
	return LEPT_PARSE_OK;

}
static int lept_parse_true(lept_context *c,lept_value *v)//解析true值
{
	EXPECT(c, 't');
	if (c->json[0] != 'r' || c->json[1] != 'u' || c->json[2] != 'e' )
	{
		return LEPT_PARSE_INVALID_VALUE;
	}
	c->json += 3;
	v->type = LEPT_TRUE;
	return LEPT_PARSE_OK;

}

static int lept_parse_false(lept_context *c, lept_value *v)//解析false值
{
	EXPECT(c, 'f');
	if (c->json[0] != 'a' || c->json[1] != 'l' || c->json[2] != 's' || c->json[3] != 'e')
	{
		return LEPT_PARSE_INVALID_VALUE;
	}
	c->json += 4;
	v->type = LEPT_FALSE;
	return LEPT_PARSE_OK;

} 

#define ISDIGIT1TO9(ch)    ((ch)>='1'&&(ch)<='9') 
#define ISDIGIT(ch)        ((ch)>='0'&&(ch)<='9')

static int lept_parse_number(lept_context *c, lept_value *v)//解析数字
{
	const char * ptc = c->json;
	if (*ptc == '-') { ptc++; }//负号处理 ，直接跳过

	if (*ptc == '0') { ptc ++ ; }//整数处理，单个0，跳过  
	else                      //不是单个0，首位1-9后面接多位数字
	{
		if (!ISDIGIT1TO9(*ptc)){ return  LEPT_PARSE_INVALID_VALUE; }
		for (; ISDIGIT(*ptc); ptc++);
	}

	if (*ptc == '.') //小数处理， 小数点后接至少一到多个数字
	{
		ptc++;
		if (!ISDIGIT1TO9(*ptc)){ return  LEPT_PARSE_INVALID_VALUE; }
		for (; ISDIGIT(*ptc); ptc++);
	}

	if (*ptc == 'e' || *ptc == 'E')//指数处理
	{
		ptc++;
		if (*ptc == '-' || *ptc == '+') { ptc++; }
		if (!ISDIGIT1TO9(*ptc)) { return  LEPT_PARSE_INVALID_VALUE; }
		for (; ISDIGIT(*ptc); ptc++);
	}

	v->u.n = strtod(c->json, NULL);//将字符串转换为浮点数，ptend指向字符串中表示浮点数的下一个位置 	
	v->type = LEPT_NUMBER;
	c->json = ptc;
	return  LEPT_PARSE_OK;

}

void lept_free(lept_value *v)//释放空间
{
	if (v->type == LEPT_STRING)
	{
		free(v->u.str.s);
	}
	else if (v->type == LEPT_ARRAY)
	{
		for (size_t i = 0; i < v->u.array.size; i++)
		{
			lept_free(&v->u.array.element[i]);
		}
		free(v->u.array.element);
	}
	else if (v->type == LEPT_OBJECT)
	{
		for (size_t i = 0; i < v->u.object.size; i++)
		{
			free(v->u.object.member[i].key);
			lept_free(&v->u.object.member[i].value);
		}
		free(v->u.object.member);
	}
	v->type = LEPT_NULL;

}
void lept_set_string(lept_value *v,const char*s,size_t len)//输入字符串到结构体中
{
	lept_free(v);
	v->u.str.s = (char*)malloc(len+1);
	if (v->u.str.s == NULL) { return; }
	
	memcpy(v->u.str.s, s, len);
	v->u.str.s[len] = '\0';
	v->u.str.len = len+1;
	v->type = LEPT_STRING;
}

void lept_set_number(lept_value *v, double n)//输入数字到结构体中
{
	v->u.n = n;
	v->type = LEPT_NUMBER;
}



#ifndef  LEPT_PARSE_STACK_INIT_SIZE
#define LEPT_PARSE_STACK_INIT_SIZE  256
#endif // ! LEPT_PARSE_STACK_INIT_SIZE


static void* lept_context_push(lept_context* c,size_t size)//文本内容压入栈中,返回数据起始的位置
{
	if (c->top + size > c->size)//压入数据后空间不够的情况
	{
		if (c->size == 0)
		{
			c->size = LEPT_PARSE_STACK_INIT_SIZE;
		}
		while (c->top + size > c->size)
		{
			c->size = c->size +(c->size >> 1);
		}
		c->stack = (char*)realloc(c->stack, c->size);
	}
	void *ret = c->stack + c->top;
	c->top += size;
	return ret;
}

static void* lept_context_top(lept_context *c,size_t size)//出栈
{
	c->top -= size;//top恢复到栈原来的位置距离
	return c->stack + c->top ;//返回要出栈数据的起始位置
}


const char *lept_parse_hex4(const char *p, unsigned *u)//解析十六进制位
{
	const char* ptemp = p;
	for (int i = 0; i < 4; i++)
	{
		if (*ptemp <= 'f'&&*ptemp >= 'a') { *u |= *ptemp - '0'; }
		else if (*ptemp <= 'F'&&*ptemp >= 'A') { *u |= *ptemp - ('A' - 10); }
		else if (*ptemp <= '9'&&*ptemp >= '0') { *u |= *ptemp - ('a' - 10); }
		else return NULL;
		ptemp++;
	}
	p = ptemp;
	return p;

}                            
#define PUTC(c,ch)  do{*(char*)lept_context_push(c,sizeof(char))=(ch) ;}while(0)
void lept_encode_utf8(lept_context*c, unsigned u)//ufs8格式的数据输入到json结构体缓冲区中（压栈保存），1字节0x7f,2字节0x7ff,3字节0x
{
	if (u <= 0x7F)//一字节
	{
		PUTC(c, u & 0xFF);
	}
	else if (u < 0x7FF)//二字节
	{
		PUTC(c, 0xC0 | ((u >> 6) & 0xFF)); //加前缀110
		PUTC(c, 0x80 | (u & 0x3F));//加前缀10
	}
	else if (u < 0xFFFF)//三字节
	{
		PUTC(c, 0xE0 | ((u >> 12) & 0xFF));
		PUTC(c, 0x80 | ((u >> 6) & 0x3F));
		PUTC(c, 0x80 | (u & 0x3F));
	}
	else if (u < 0x10FFFF)
	{
		PUTC(c, 0XF0 |((u >> 18) & 0xFF));
		PUTC(c, 0x80 |((u >> 12) & 0x3F));
		PUTC(c, 0x80 |((u >> 6) & 0x3F));
		PUTC(c, 0x80 | (u  & 0x3F));
	}

}

static int lept_parse_array(lept_context *c, lept_value *v)//解析数组
{
	int size = 0;
	int ret = 0;
	EXPECT(c,'[');
	if (*c->json == ']') //解析完成，空数组
	{
		c->json++;
		v->u.array.size = 0;
		v->u.array.element = NULL;
		v->type = LEPT_ARRAY;
		return LEPT_PARSE_OK;
	}
	while (1)
	{
		
		lept_value vtemp;//临时值
	   lept_init(&vtemp);
		
		
		if ((ret=lept_parse_value(c,&vtemp)) != LEPT_PARSE_OK)             //解析数组里面的数值
		{
			break;
		}
		memcpy((lept_value *)lept_context_push(c, sizeof(lept_value)),&vtemp, sizeof(lept_value));//vtemp压入栈中
		size++;
		lept_parse_whitespace(c);

		if (*c->json == ',')//继续解析下一个值
		{
			c->json++;
			lept_parse_whitespace(c);//解析空白
		}
		else if (*c->json == ']')//解析完毕
		{
			c->json++;
			v->u.array.size = size;
			size*= sizeof(lept_value);
			v->u.array.element =(lept_value*)malloc(size);
			memcpy(v->u.array.element, lept_context_top(c, size), size); //栈中所有数据复制到element中
			
			v->type = LEPT_ARRAY;
			return LEPT_PARSE_OK;
		}
		else  //解析出错
		{
			ret= LEPT_PARSE_MISS_COMMA_OR_SQUARE_BRACKET;
			break;
		}
	/*size_t i, size = 0;
	int ret;
	EXPECT(c, '[');
	lept_parse_whitespace(c);
	if (*c->json == ']') {
		c->json++;
		v->type = LEPT_ARRAY;
		v->u.array.size = 0;
		v->u.array.element = NULL;
		return LEPT_PARSE_OK;
	}
	for (;;) {
		lept_value e;
		lept_init(&e);
		if ((ret = lept_parse_value(c, &e)) != LEPT_PARSE_OK)
			break;
		memcpy(lept_context_push(c, sizeof(lept_value)), &e, sizeof(lept_value));
		size++;
		lept_parse_whitespace(c);
		if (*c->json == ',') {
			c->json++;
			lept_parse_whitespace(c);
		}
		else if (*c->json == ']') {
			c->json++;
			v->type = LEPT_ARRAY;
			v->u.array.size = size;
			size *= sizeof(lept_value);
			memcpy(v->u.array.element = (lept_value*)malloc(size), lept_context_top(c, size), size);
			return LEPT_PARSE_OK;
		}
		else {
			ret = LEPT_PARSE_MISS_COMMA_OR_SQUARE_BRACKET;
			break;
		}*/
	}
	for (int i = 0; i < size; i++)
	{
		lept_free((lept_value*)lept_context_top(c, sizeof(lept_value)));
	}
	return ret;
}

static int lept_parse_string(lept_context *c, lept_value *v)//解析字符串
{
	size_t head = c->top;
	size_t len = 0;
	EXPECT(c, '\"');
	const char *ptc = c->json;//指向文本存储的内容
	while (1)
	{
		char ch = *ptc++;
		switch (ch)
		{
		case '\"'://字符串结尾的情况
			len = c->top - head;
			lept_set_string(v, (const char*)lept_context_top(c,len),len);
			c->json = ptc;
			return LEPT_PARSE_OK;

		case '\0':  //空字符 
			c->top = head;
		
		case '\\':
			switch (*ptc++)
			{
			case '\"': PUTC(c, '\\'); break;
			case '\\': PUTC(c, '\\'); break;
			case '/':  PUTC(c, '/'); break;
			case 'b':  PUTC(c, '\b'); break;
			case 'f':  PUTC(c, '\f'); break;
			case 'n':  PUTC(c, '\n'); break; 
			case 'r':  PUTC(c, '\r'); break;
			case 't':  PUTC(c, '\t'); break;
			case 'u':
				unsigned utemp;
				if (!(ptc = lept_parse_hex4(ptc, &utemp)))//非法十六进制
				{
					return LEPT_PARSE_INVALID_UNICODE_HEX;
				}
				lept_encode_utf8(c,utemp);

			default:
				c->top = head;
				return LEPT_PARSE_INVALID_STRING_CHAR;
			}
		default: 
			*(char*)lept_context_push(c,sizeof(char))=ch;//将json文本内容压入栈中 
		}
	}
}
static int lept_parse_object(lept_context*c, lept_value *v)
{
	int ret = 0;
	int size = 0;
	lept_member mem;
	lept_init(&mem.value);
	EXPECT(c,'{');
	lept_parse_whitespace(c);//解析空格
	if (*c->json == '}') //空对象
	{
		mem.key = NULL;
		mem.keylen = 0;
		mem.value.type = LEPT_NULL;
		v->u.object.member = &mem;
		v->u.object.size = 0;
		v->type = LEPT_OBJECT;
		return LEPT_PARSE_OK;
	}
	while (1)
	{
		
		lept_init(&mem.value);
		if ((ret = lept_parse_value(c, v)) != LEPT_PARSE_OK)
		{
			break;
		}

		if (v->type != LEPT_STRING)//关键字不是字符串
		{
			ret=  LEPT_PARSE_MISS_KEY;
			break;
		}

		mem.keylen = v->u.str.len;
		mem.key = (char*)malloc(mem.keylen);//分配
		memcpy(mem.key, v->u.str.s,mem.keylen);
		

		if (*c->json != ':')
		{
			ret= LEPT_PARSE_MISS_COLON;
			break;
		}
		c->json++;
		lept_parse_whitespace(c);//解析空白

		if ((ret = lept_parse_value(c, &mem.value)) != LEPT_PARSE_OK)//判断value值
		{
			break;
		}
		
		memcpy(lept_context_push(c,sizeof(lept_member)),&mem,sizeof(lept_member));//value压栈	
		size++;
		mem.key = NULL;

		if (*c->json == '}') // 解析到末尾，弹出缓冲组
		{
			int SIZE = size * sizeof(lept_member);
			v->u.object.member = (lept_member*)malloc(SIZE);//分配大小
			memcpy(v->u.object.member,lept_context_top(c, SIZE),SIZE);//出栈，数值复制
			v->type = LEPT_OBJECT;
			v->u.object.size = size;
			c->json++;
	
			return LEPT_PARSE_OK;
		}

		else if (*c->json == ',')//继续解析
		{
			c->json++;
			lept_parse_whitespace(c);
		}
		else
		{
			ret= LEPT_PARSE_MISS_COMMA_OR_CURLY_BRACKET;
			break;
		}
		
	}

	/*解析出错，释放分配的空间*/
	free(mem.key);//释放分配给mem成员变量key的内存
	for (int i = 0; i < size; i++)//释放栈中分配给member的内存
	{
		lept_member *ptmem = (lept_member*)lept_context_top(c, sizeof(lept_member));
		free(ptmem->key);
		lept_free(&ptmem->value);
	}
	v->type = LEPT_NULL;
	return ret;
	
}

/*根据json字符串首个字符来判断是哪种结构*/
int lept_parse_value(lept_context *c , lept_value *v)//对c中的字符串内容进行解析，存放到json结构体v中
{
	switch (*c->json)
	{
	  case 'n':return lept_parse_null(c, v);//null值解析
	  case 't':return lept_parse_true(c, v);//true解析
	  case 'f':return lept_parse_false(c, v);//false解析
	  case '\0':return LEPT_PARSE_EXPECT_VALUE; //空白值
	  case '"':return  lept_parse_string(c, v);//解析字符串
	  case '[':return lept_parse_array(c, v);//解析数组
	  case '{':return lept_parse_object(c, v);//解析对象
	  default:return lept_parse_number(c,v);//数字解析
	}
 
}

/*接口功能： 输入json字符串,转换为json结构体,结果存放在 v 中*/
int lept_parse(lept_value*v, const char *json)
{
	lept_context  c;
	c.json = json;
	c.stack = NULL;
	c.size = c.top = 0;
   
	lept_parse_whitespace(&c);//解析空白
	int ret;
	if ((ret=lept_parse_value(&c, v)) != LEPT_PARSE_OK)//判断no_sigular，json 在一个值之后，空白之后还有其它字符
	{
		return ret;
	}
	lept_parse_whitespace(&c);
	if (*c.json != '\0')
	{
		ret = LEPT_PARSE_ROOT_NOT_SINGULAR;
	}
	if (v->type == LEPT_STRING || v->type == LEPT_ARRAY || v->type == LEPT_OBJECT)//需要动态释放内存的情况
	{
		free(c.stack);//释放分配给stack的内存
	}
	return ret;
	
}

/*####提供访问json结构体信息的接口#####*/

/*获取json结构体的类型*/
lept_type lept_get_type(const lept_value *v)//json类型
{
	return v->type;
}

/*获取json结构体的数字*/
double lept_get_number(const lept_value *v)//数字
{
	return v->u.n;
}

size_t lept_get_array_size(const lept_value *v)//得到json保存的数组大小
{
	return v->u.array.size;
}

lept_value *lept_get_element(const lept_value*v,int cno)//数组元素
{
	return &v->u.array.element[cno-1];
}

char *lept_get_string_firstadd(const lept_value *v)//字符串首地址
{
	return v->u.str.s;
}
size_t lept_get_string_size(const lept_value*v)//字符串大小
{
	return v->u.str.len;
}

/**初始化json结构体**/
void lept_init(lept_value *v)
{
	v->type = LEPT_NULL;
}

size_t lept_get_object_size(const lept_value *v)
{
	return v->u.object.size;
}

lept_member *lept_get_object_member(const lept_value *v, int cno)
{
	return &v->u.object.member[cno - 1];
}





int lept_stringly_string(lept_context *c, const lept_value *v)//内容保存在栈中
{
	int len = v->u.str.len-1;
	memcpy((char*)lept_context_push(c, len), v->u.str.s, len);
	return len;

}

int lept_stringly_number(lept_context *c, const lept_value *v)
{
	char cnum[20];
	int len = sprintf(cnum,"%.0lf",v->u.n);
	memcpy((char*)lept_context_push(c, len), cnum, len);
	return len;
}

int lept_stringly_true(lept_context *c)
{
	char ptc[5] = "true";
	memcpy((char*)lept_context_push(c, 4), ptc, 4);
	return 4;
}

int lept_stringly_array(lept_context *c, const lept_value *v)
{
	int slen = 0;
	int arraysize = v->u.array.size;
	*(char*)lept_context_push(c, 1) = '[';
	slen += 1;
	for (int i = 0; i < arraysize; i++)
	{
		slen+=lept_stringly_value(c, &v->u.array.element[i]);
		if (i == arraysize - 1) { break; }
		*(char*)lept_context_push(c,1)=',';
		slen += 1;
	}
	*(char*)lept_context_push(c, 1) = ']';
	slen += 1;
	
	return slen;
}

int lept_stringly_object(lept_context *c, const lept_value *v)
{
	int slen = 0;
	int objectsize = v->u.object.size;
	*(char*)lept_context_push(c, 1) = '{';
	slen += 1; 

	for (int i = 0; i < objectsize; i++)
	{
		int len = v->u.object.member[i].keylen - 1;
		memcpy((char*)lept_context_push(c, len), v->u.object.member[i].key, len);  //关键字：键值
		slen += len;

		*(char*)lept_context_push(c, 1) = ':';
		slen += 1;

		slen += lept_stringly_value(c, &v->u.object.member[i].value);

		if (i == objectsize - 1) { break; }
		*(char*)lept_context_push(c, 1) = ',';
		slen += 1;

	}
	*(char*)lept_context_push(c, 1) = '}';
	slen += 1;
	return slen;

}


int lept_stringly_value(lept_context *c, const lept_value *v)
{
	switch (v->type)
	{
	case LEPT_STRING: return lept_stringly_string(c, v);
	case LEPT_NUMBER: return lept_stringly_number(c, v);
	case LEPT_OBJECT: return lept_stringly_object(c, v);
	case LEPT_ARRAY:  return lept_stringly_array(c, v);
	case LEPT_TRUE:  return lept_stringly_true(c);

	default:return 0;
		break;
	}
}

/*json结构体格式化 输出为字符串形式*/
char *lept_stringly(const lept_value *v)
{
	
	if (v->type == LEPT_NULL)
	{
		return NULL;
	}
	lept_context c;
	c.stack = NULL;
	c.size = c.top = 0;
	size_t strlen=lept_stringly_value(&c, v);//c缓存栈中数据

	*(char*)lept_context_push(&c, 1) = '\0';
	strlen += 1;

	char *ptc=(char*)lept_context_top(&c,strlen);
	char *cret = (char*)malloc(strlen);
	
	memcpy(cret,ptc,strlen);
	free(c.stack);
	return cret;
}
