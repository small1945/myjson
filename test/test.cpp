#define _CRT_SECURE_NO_WARINGS

#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include"json.h"
#define CRTDBG_MAP_ALLOC  
#include <stdlib.h>  

static int test_count = 0;
static int test_pass = 0;
static int main_ret = 0;
#define EXPECT_EQ_BASE(equality, expect, actual, format) \
    do {\
        test_count++;\
        if (equality)\
            test_pass++;\
        else {\
            fprintf(stderr, "%s:%d: expect: " format " actual: " format "\n", __FILE__, __LINE__, expect, actual);\
            main_ret = 1;\
        }\
    } while(0)

#define EXPECT_EQ_INT(expect,actual)  EXPECT_EQ_BASE((expect)==(actual),expect,actual,"%d")

static void test_parse_null()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v, "null"));
	EXPECT_EQ_INT(LEPT_NULL, lept_get_type(&v));
}

static void test_parse_not_singular()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_ROOT_NOT_SINGULAR, lept_parse(&v, "  truex  "));
}


static void test_parse_true()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v, "true"));
	EXPECT_EQ_INT(LEPT_TRUE, lept_get_type(&v));
}
/*���ֲ���*/
#define EXPECT_EQ_DOUBLE(expect,actual) EXPECT_EQ_BASE((expect)==(actual),expect,actual,"%f")
#define TEST_NUMBER(expect, json)\
    do {\
        lept_value v;\
        EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v, json));\
        EXPECT_EQ_INT(LEPT_NUMBER, lept_get_type(&v));\
        EXPECT_EQ_DOUBLE(expect, lept_get_number(&v));\
    } while(0)

//�ַ�������
#define EXPECT_EQ_STRING(expect,actual,length)\
        EXPECT_EQ_BASE((sizeof(expect)==length)&&memcmp(expect,actual,length)==0,expect,actual,"%s")

#define TEST_STRING(expect,json)\
	do{\
      lept_value v;\
	  EXPECT_EQ_INT(LEPT_PARSE_OK,lept_parse(&v,json));\
	  EXPECT_EQ_INT(LEPT_STRING,lept_get_type(&v));\
      EXPECT_EQ_STRING(expect,lept_get_string_firstadd(&v),lept_get_string_size(&v));\
      lept_free(&v);\
	}while(0)




static void test_parse_array()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v, "[\"a\",\"b\",\"c\",\"d\"]"));
	EXPECT_EQ_INT(LEPT_ARRAY, lept_get_type(&v));
	EXPECT_EQ_INT(4,lept_get_array_size(&v));
	EXPECT_EQ_INT(LEPT_STRING, lept_get_type(lept_get_element(&v,1)));
	//EXPECT_EQ_INT(LEPT_STRING, lept_get_type(lept_get_element(&v, 2)));
	EXPECT_EQ_INT(LEPT_STRING, lept_get_type(lept_get_element(&v, 2)));
	EXPECT_EQ_STRING("a", lept_get_string_firstadd(lept_get_element(&v, 1)), lept_get_string_size(lept_get_element(&v, 1)));
	EXPECT_EQ_STRING("b", lept_get_string_firstadd(lept_get_element(&v, 2)),lept_get_string_size(lept_get_element(&v, 2)));
	//lept_free(&v);
}

static void test_parse_object()
{
	lept_value v;
	lept_member m;
	EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v,"{\"a\":\"value\",\"b\":123,\"c\":456,\"d\":456}"));
	EXPECT_EQ_INT(LEPT_OBJECT, lept_get_type(&v));
	EXPECT_EQ_INT(4,lept_get_object_size(&v));

	m = *lept_get_object_member(&v, 1);
	EXPECT_EQ_STRING("a", m.key, m.keylen);
	EXPECT_EQ_INT(LEPT_STRING, lept_get_type(&m.value));
	EXPECT_EQ_STRING("value", lept_get_string_firstadd(&m.value), lept_get_string_size(&m.value));
	
	m = *lept_get_object_member(&v, 2);
	EXPECT_EQ_STRING("b", m.key, m.keylen);
	EXPECT_EQ_INT(LEPT_NUMBER, lept_get_type(&m.value));
	EXPECT_EQ_INT(123, lept_get_number(&m.value));


	m = *lept_get_object_member(&v, 3);
	EXPECT_EQ_STRING("c", m.key, m.keylen);
	EXPECT_EQ_INT(LEPT_NUMBER, lept_get_type(&m.value));
	EXPECT_EQ_INT(456, lept_get_number(&m.value));
	
	m = *lept_get_object_member(&v, 4);
	EXPECT_EQ_STRING("d", m.key, m.keylen);
	EXPECT_EQ_INT(LEPT_NUMBER, lept_get_type(&m.value));
	EXPECT_EQ_INT(456, lept_get_number(&m.value));

	free(v.u.object.member[0].key);
	free(v.u.object.member[1].key);
	free(v.u.object.member[2].key);
	//free(v.u.object.member[3].key);
	free(v.u.object.member);
	//lept_free(&v);
	
	
}

static void test_parse_false()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_OK, lept_parse(&v, "false"));
	EXPECT_EQ_INT(LEPT_FALSE, lept_get_type(&v));
}

static void test_parse_expect()
{
	lept_value v;
	EXPECT_EQ_INT(LEPT_PARSE_EXPECT_VALUE, lept_parse(&v, "     "));
}


//������� 
#define TEST_ERROR(error,json)\
	do{\
	   lept_value v;\
       EXPECT_EQ_INT(error,lept_parse(&v,json));\
	}while(0)


static void test_parse_number()
{
	TEST_NUMBER(0.0, "1");
	TEST_NUMBER(1.2341E-20, "1.234E-20");
	TEST_NUMBER(0.0, "0");
	TEST_NUMBER(0.0, "-0");
	TEST_NUMBER(0.0, "-0.0");
	TEST_NUMBER(1.0, "1");
	TEST_NUMBER(-1.0, "-1");
	TEST_NUMBER(1.5, "1.5");
	TEST_NUMBER(-1.5, "-1.5");
	TEST_NUMBER(3.1416, "3.1416");
	TEST_NUMBER(1E10, "1E10");
	TEST_NUMBER(1e10, "1e10");
	TEST_NUMBER(1E+10, "1E+10");
	TEST_NUMBER(1E-10, "1E-10");
	TEST_NUMBER(-1E10, "-1E10");
	TEST_NUMBER(-1e10, "-1e10");
	TEST_NUMBER(-1E+10, "-1E+10");
	TEST_NUMBER(-1E-10, "-1E-10");
	TEST_NUMBER(1.234E+10, "1.234E+10");
	TEST_NUMBER(1.234E-10, "1.234E-10");
	TEST_NUMBER(0.0, "1e-10000");
	TEST_ERROR(LEPT_PARSE_INVALID_VALUE, "-10");
	TEST_ERROR(LEPT_PARSE_INVALID_VALUE, "-100");

}





static void test_parse_string()
{
	TEST_STRING("HELLO","\"HELLO\"");
	TEST_STRING("LEO","\"LEO\"");
	TEST_STRING("hello\n","\"hello\n\"");
	

}

static void test_stringly_number()
{
	lept_value v;
	lept_parse(&v,"500");
	EXPECT_EQ_STRING("500", lept_stringly(&v),3);
}
static void test_stringly_string()
{
	lept_value v;
	lept_parse(&v, "\"asd\"");
	char *ptemp = lept_stringly(&v);
	EXPECT_EQ_STRING("asd", ptemp, 4);
	free(ptemp);
	ptemp = nullptr;
}
static void test_stringly_true()
{
	lept_value v;
	lept_parse(&v, "true");
	char *ptemp = lept_stringly(&v);
	EXPECT_EQ_STRING("true", ptemp,5);
	free(ptemp);
	ptemp = nullptr;
}

static void test_stringly_array()
{
	lept_value v;
	lept_parse(&v, "[5,6,7,8]");
	char *ptemp = lept_stringly(&v);
	
	EXPECT_EQ_STRING("[5,6,7,8]", ptemp,strlen(ptemp)+1);
	free(ptemp);
	ptemp = nullptr;

}

static void test_stringly_object()
{
	lept_value v;
	lept_parse(&v, "{\"a\":\"value\",\"b\":123,\"c\":456,\"d\":[\"a\",50]}");
	char *ptemp= lept_stringly(&v);
	EXPECT_EQ_STRING("{a:value,b:123,c:456,d:[a,50]}", ptemp, strlen(ptemp) + 1);
	free(ptemp);
	ptemp = nullptr;

}

static void test_stringly()
{
	test_stringly_true();
	test_stringly_number();
	test_stringly_array();
	test_stringly_string();
	test_stringly_object();
}


static void test_func()
{
	test_parse_null();
	test_parse_not_singular();
	test_parse_true();
	test_parse_false();
	test_parse_expect();
	//test_parse_number();
	test_parse_string();
	test_parse_array();
	test_parse_object();
	test_stringly();

}

int main()
{
	test_func();
	printf("%d/%d passed\n", test_pass, test_count);
	return main_ret; 
}